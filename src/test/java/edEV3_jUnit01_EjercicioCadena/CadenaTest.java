package edEV3_jUnit01_EjercicioCadena;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CadenaTest {
	Cadena cadena;

	
	@BeforeEach
	void setUp() throws Exception {
		
		cadena = new Cadena();
		
	}

	

	@Test
	void testLongitud() {
		int expected = 5;
		int actual = cadena.longitud("Adios");
		assertEquals(expected, actual , "Contando caracteres");
		
	}
	
	@Test
	void testLongitudFail() {
		int expected = 5;
		int actual = cadena.longitud("Adios");
		assertEquals("", actual , "Contando caracteres error");
		
	}
	
	@Test
	void testVocales() {
		int expected = 2;
		int actual = cadena.vocales("Hola");
		assertEquals(expected, actual , "Contando vocales");
		
	}
	
	@Test
	void testVocalesFail() {
		int expected = 2;
		int actual = cadena.vocales("Hola");
		assertEquals("", actual , "Contando vocales error");
		
	}
	
	@Test
	void testInvertir() {
		String expected = "aloH";
		String actual = cadena.invertir("Hola");
		
		assertEquals(expected, actual , "Cadena al rev�s");
		
	}
	
	@Test
	void testInvertirFail() {
		String expected = "aloH";
		String actual = cadena.invertir("Hola");
		assertEquals("", actual , "Cadena al rev�s error");
		
	}
	
	@Test
	void testContarLetra() {
		int expected = 3;
		int actual = cadena.contarLetra("Palabra",'a');
		assertEquals(expected, actual , "Cadena al rev�s");
		
	}
	
	@Test
	void testContarLetraFail() {
		int expected = 3;
		int actual = cadena.contarLetra("Palabra",'a');
		assertEquals("", actual , "Cadena al rev�s error");
		
	}

}
