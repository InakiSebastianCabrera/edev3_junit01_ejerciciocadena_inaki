package edEV3_jUnit01_EjercicioCadena;

public class Cadena {
	public int longitud(String cadena) {
		return cadena.length();
	}
	public int vocales(String cadena) {
		int contador = 0;
		
		for (int i = 0; i < cadena.length(); i++) {
			char letra = cadena.charAt(i);
			if (letra == 'a' || letra == 'e' || letra == 'i' || letra == 'o' || letra == 'u') {
				contador ++;
			}
			
		}
		
		
		return contador;
	}
	public String invertir(String cadena) {
		String cadenaInvertida = "";
		
		for (int i = cadena.length() - 1; i >= 0; i--) {
			cadenaInvertida += cadena.charAt(i);
		}
		
		return cadenaInvertida;
	}
	
	
	public int contarLetra(String cadena,char caracter) {
		int contador = 0;
		for (int i = 0; i < cadena.length(); i++) {
			if (cadena.charAt(i) == caracter) {
				contador ++;
			}
		}
		return contador;
	}
}
